//
//  RetriveVC.swift
//  Cassizz
//
//  Created by TuanMudaSena on 12/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import SwiftyJSON


class RetriveVC: UIViewController {

    @IBOutlet var contentView: UIView!
   

//    f1a6dd1c473bc4ed88ff37f54d757cb3
//    54ce20285234a479b4b9fbef21ac9145 godspeed
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBAction func forgetPassword(sender: AnyObject) {
        
        
        let email:NSString = txtEmail.text!
        
        var readyRegis:Bool = true
        
        
        if ( email.isEqualToString("")) {
            
            let alertView = UIAlertController(title: "Sign up Failed!", message: "Please fill the blank text", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
            self.presentViewController(alertView, animated: true){}
            readyRegis=false
            
        }
        
        
        if(readyRegis){
            
            do{

                
                
                let post:NSString = "email=\(email)"
                
                NSLog("PostData: %@",post);
                
                let url:NSURL = NSURL(string:UrlService.retrivePassword)!
                
                let postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
                
                let postLength:NSString = String( postData.length )
                
                
                let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "POST"
                
                
                request.HTTPBody = postData
                request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("assis", forHTTPHeaderField: "assis")
                
                
                LoadingHUD.showLoadingHUD(contentView)
                Alamofire.request(request).responseJSON {
                    response in
                    print(response.result)
                    switch response.result {
                    case .Success(let data):
                        let json = JSON(data)
                        print(json)
                        let status = json["status"].boolValue
                        if(status)
                        {
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: json["msg"].stringValue, message: "Password baru telah dikirim ke email anda", preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default,handler:
                                { (action: UIAlertAction!) in
                                    
                                    
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                    
                            }))
                            self.presentViewController(alertView, animated: true){}
                            
                        }
                        else
                        {
                            
                            
                            let alertView = UIAlertController(title: "Reset password failed!", message: json["msg"].stringValue, preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
                            self.presentViewController(alertView, animated: true){}
                            
                        }
                        
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                }
                
            }
            
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
