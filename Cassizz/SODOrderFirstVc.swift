//
//  SODOrderFirstVc.swift
//  Cassizz
//
//  Created by TuanMudaSena on 9/4/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit

class SODOrderFirstVc: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate,UIScrollViewDelegate{

    @IBOutlet var pickFromList : UITextField!
    @IBOutlet var carRegNumTxt: UITextField!
    
    @IBOutlet var carBrandList: UITextField!

    @IBOutlet var carYearTxt: UITextField!
    
    @IBOutlet var transmissionList: UITextField!
    
    @IBOutlet var engineTypeList: UITextField!
    
    @IBOutlet var pickupDate: UITextField!
    
    @IBOutlet var timeList: UITextField!
    
    @IBOutlet var complaintTxt: UITextField!
    
    @IBOutlet var lubricantBrandList: UITextField!
    
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var uiView: UIView!
    
    let data = ["Item1", "Item2", "Item3"]
    
    var picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        pickFromList.inputView = picker
    
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
      
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickFromList.text = data[row]
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (scrollView.contentOffset.x > 0 || scrollView.contentOffset.x < 0 ){
            scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
