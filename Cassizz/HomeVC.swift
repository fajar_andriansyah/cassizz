//
//  ViewController.swift
//  Cassizz
//
//  Created by TuanMudaSena on 8/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var UsernameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.performSegueWithIdentifier("goto_login", sender: self)
        // Do any additional setup after loading the view, typically from a nib.
        UINavigationBar.appearance().barTintColor = UIColor.blueColor()
        UINavigationBar.appearance().tintColor = UIColor.blackColor()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        let isLoggedIn: Int = User.getLOGGEDIN()!
        if(isLoggedIn != 1){
            self.performSegueWithIdentifier("goto_login", sender: self)
        }else{
//            self.UsernameLbl.text = prefs.valueForKey("USERNAME") as? String
        }
        
//        self.navigationController?.navigationBarHidden = true
        
        
        
//        self.performSegueWithIdentifier("goto_login", sender: self)
        
    }
    
    @IBAction func LogoutTapped(sender: UIButton) {
        self.performSegueWithIdentifier("goto_login", sender: self)
    }

}

