//
//  UrlService.swift
//  Cassizz
//
//  Created by TuanMudaSena on 27/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import Foundation

class UrlService {
    static let retrivePassword : String = "http://mobiledev.casizz.com/rest_server/resetPassword/format/json"
    static let changePassword : String = "http://mobiledev.casizz.com/rest_server/saveChangePassword/format/json"
    static let login : String = "http://mobiledev.casizz.com/rest_server/login/format/json"
    static let signUp : String = "http://mobiledev.casizz.com/rest_server/tambahUser/format/json"
    static let pickFromList : String = "http://mobiledev.casizz.com/rest_server/getPickFromList/format/json/id_user/"
    static let brandCar : String = "http://mobiledev.casizz.com/rest_server/getPickFromList/format/json/id_user/"
    static let typeCar : String = "http://mobiledev.casizz.com/rest_server/tipeMobil/format/json/id_merk"
    static let oil : String = "http://mobiledev.casizz.com/rest_server/getLubricant/format/json/tahun/"
//    Service menampilkan list bengkel berdasarkan koordinat dan jenis service yg dipilih customer (untuk background link path nya adalah : http://mobile.casizz.com/upload/bengkel/)
    static let listBengkel : String = "http://mobiledev.casizz.com/rest_server/listBengkel/format/json/jenisService/lat/lon/"
//    Service menampilkan list item servis mobil
    static let listItemService : String = "http://mobiledev.casizz.com/rest_server/getItemService/format/json/id_bengkel/km/id_oli/jenisService"
//    Service input order bengkel
    static let orderBengkel : String = "http://mobiledev.casizz.com/rest_server/orderBengkel/format/json"
//    Service list order by id user (untuk link foto ialah : http://mobile.casizz.com/upload/user_bengkel/)
    static let listOrderByUser : String = "http://mobiledev.casizz.com/rest_server/getOrderIdUser/format/json/id_user/"
//    Service input konfirmasi customer
    static let conCust : String = "http://mobiledev.casizz.com/rest_server/aksiPaBengkelCek/format/json"
//    Service untuk menampilkan no rekening dan invoice
    static let invoice:String = "http://mobiledev.casizz.com/rest_server/customerPayInvoice/format/json/id_order/"
//    Service konfirmasi kedatangan mobil
    static let confirmCar : String = "http://mobiledev.casizz.com/rest_server/konfirmasiMobilUser/format/json"
//    Service untuk update profile
    static let updateProf : String = "http://mobiledev.casizz.com/rest_server/updateProfile/format/json"
    
}