//
//  ChangePasswordVC.swift
//  Cassizz
//
//  Created by TuanMudaSena on 13/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit
import Alamofire
import TextFieldEffects
import SwiftyJSON
import MBProgressHUD

class ChangePasswordVC: UIViewController {

    @IBOutlet var txtNewPassword: UITextField!
    
    @IBOutlet var txtConfirmPassword: UITextField!
    
    @IBOutlet var contentView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    @IBAction func changePassTapped(sender: UIButton) {
        let newPassword:NSString = txtNewPassword.text!
        let confPassword:NSString = txtConfirmPassword.text!
        var flagPass:Bool = true
        let userId : NSString = User.getId()!
        
        if ( newPassword.isEqualToString("") || confPassword.isEqualToString("")){
            let alertView = UIAlertController(title: "Change Password Failed!", message: "Please fill the blank text", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
            self.presentViewController(alertView, animated: true){}
            flagPass=false
        }
        
        if (!newPassword.isEqualToString(confPassword as String)){
            let alertView = UIAlertController(title: "Change Password !", message: "Password not match", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
            self.presentViewController(alertView, animated: true){}
            flagPass=false
        }
        print(newPassword.isEqualToString(confPassword as String))
        print(!newPassword.isEqualToString(confPassword as String))
        
        if(flagPass){
            
            do{
//                let passwordMd5:NSString = md5(string:newPassword as String)
                let post:NSString = "id_user=\(userId)&password=\(newPassword)"
                
                let url:NSURL = NSURL(string: UrlService.changePassword )!
                let postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
                let postLength:NSString = String( postData.length )
                
                let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "POST"
                request.HTTPBody = postData
                request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("assis", forHTTPHeaderField: "assis")
                
            
                LoadingHUD.showLoadingHUD(contentView)
                Alamofire.request(request).responseJSON {
                    response in
                    print(response.result)
                    switch response.result {
                    case .Success(let data):
                        let json = JSON(data)
                        print(json)
                        let status = json["status"].boolValue
                        if(status)
                        {
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: "Change Password Success!", message: "Password has been Changed", preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default,handler:
                                { (action: UIAlertAction!) in
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                    
                            }))
                            self.presentViewController(alertView, animated: true){}
                            
                        }
                        else
                        {
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: "Change Password Failed!", message: json["msg"].stringValue, preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
                            self.presentViewController(alertView, animated: true){}
                            
                        }
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                }
                
            }
            
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
