//
//  PackagedCell.swift
//  Cassizz
//
//  Created by TuanMudaSena on 17/4/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit

class PackagedCell: UITableViewCell {
    
    @IBOutlet var pickUpTitleLabel: UILabel!
    @IBOutlet var lubServiceBtn: UIButton!
    
    @IBOutlet var lubTitleLabel: UILabel!
    @IBOutlet var RegServiceBtn: UIButton!
    
    let titleLabel1 : CGFloat = 80
    var frameAdded = false
    
    
    class var expandedHieght: CGFloat {
        get {
            return 300
        }
    }
    class var defaultHieght: CGFloat {
        get {
            return 80
        }
    }
    
    func checkHeight(){
        lubServiceBtn.hidden = (frame.size.height < PackagedCell.expandedHieght)
        lubTitleLabel.hidden = (frame.size.height < PackagedCell.expandedHieght)
        RegServiceBtn.hidden = (frame.size.height < PackagedCell.expandedHieght)
        
    }
    
    func watchFrameChanges(){
        if !frameAdded {
            addObserver(self, forKeyPath: "frame", options: .New, context: nil)
            frameAdded = true
        }
        
        checkHeight()
    }
    
    func ignoreFramesChanges(){
        if frameAdded {
            removeObserver(self, forKeyPath: "frame")
            frameAdded = false
        }
        
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "frame"{
            checkHeight()
        }
    }
    
    deinit {
        print("CALLING BACK")
        ignoreFramesChanges()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
