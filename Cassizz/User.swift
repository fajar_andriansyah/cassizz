//
//  User.swift
//  Cassizz
//
//  Created by TuanMudaSena on 14/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import Foundation

class User {
    
    class func setData(value : [String : AnyObject]) {
        
        if let currentData = value["id"] {
            if let currentValue = Converter.toString(currentData) {
                self.setId(currentValue)
            }
        }
        
        if let currentData = value["address"] {
            if let currentValue = Converter.toString(currentData) {
                self.setAddress(currentValue)
            }
        }
        
        if let currentData = value["email"] {
            if let currentValue = Converter.toString(currentData) {
                self.setEmail(currentValue)
            }
        }
        
        if let currentData = value["name"] {
            if let currentValue = Converter.toString(currentData) {
                self.setName(currentValue)
            }
        }
        
        if let currentData = value["phone"] {
            if let currentValue = Converter.toString(currentData) {
                self.setPhone(currentValue)
            }
        }
        
    }
    
    //ID
    class func getId()->String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let value = userDefaults.valueForKey("casizz-id") as? String {
            return value
        }
        return nil
    }
    
    class func setId(value : String) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(value, forKey: "casizz-id")
        userDefaults.synchronize()
    }
    
    class func removeId() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("casizz-id")
        userDefaults.synchronize()
    }

    //EMAIL
    class func getEmail()->String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let value = userDefaults.valueForKey("casizz-email") as? String {
            return value
        }
        return nil
    }
    
    class func setEmail(value : String) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(value, forKey: "casizz-email")
        userDefaults.synchronize()
    }
    
    class func removeEmail() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("casizz-email")
        userDefaults.synchronize()
    }
    
    //ADDRESS
    class func getAddress()->String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let value = userDefaults.valueForKey("casizz-address") as? String {
            return value
        }
        return nil
    }
    
    class func setAddress(value : String) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(value, forKey: "casizz-address")
        userDefaults.synchronize()
    }
    
    class func removeAddress() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("casizz-address")
        userDefaults.synchronize()
    }
    
    //Name
    class func getName()->String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let value = userDefaults.valueForKey("casizz-name") as? String {
            return value
        }
        return nil
    }
    
    class func setName(value : String) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(value, forKey: "casizz-name")
        userDefaults.synchronize()
    }
    
    class func removeName() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("casizz-name")
        userDefaults.synchronize()
    }
    
    //PHONE
    class func getPhone()->String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let value = userDefaults.valueForKey("casizz-phone") as? String {
            return value
        }
        return nil
    }
    
    class func setPhone(value : String) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(value, forKey: "casizz-phone")
        userDefaults.synchronize()
    }
    
    class func removePhone() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("casizz-phone")
        userDefaults.synchronize()
    }
    
    
    //FLAG LOGIN SEMENTARA
    class func getLOGGEDIN()->Int? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let value = userDefaults.valueForKey("ISLOGGEDIN") as? Int {
            return value
        }
        return nil
    }
    
    class func setLOGGEDIN() {
        let prefs : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setValue(1, forKey: "ISLOGGEDIN")
        
    }
    
    
    
    
    
}


