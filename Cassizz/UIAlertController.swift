//
//  UIAlertController.swift
//  Cassizz
//
//  Created by Sena on 3/30/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    class func alertControllerWithTitle(title:String, message:String) -> UIAlertController {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        controller.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        return controller
    }
}