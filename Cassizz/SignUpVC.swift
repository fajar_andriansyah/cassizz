//
//  SignUpVC.swift
//  Cassizz
//
//  Created by TuanMudaSena on 8/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit
import Alamofire
import TextFieldEffects
import SwiftyJSON
import MBProgressHUD

class SignUpVC: UIViewController {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var regisButton: UIButton!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func regisTapped(sender: UIButton) {
        
  
        let username:NSString = txtUserName.text!
        let password:NSString = txtPassword.text!
        let phone:NSString = txtPhone.text!
        let name:NSString = txtName.text!
        let confPassword:NSString = txtConfirmPassword.text!
        
//        let username:NSString = "holaca@gmail.com"
//        let password:NSString = "1234"
//        let phone:NSString = "123456"
//        let name:NSString = "test"
//        let confPassword:NSString = "1234"

        
        var readyRegis:Bool = true
    
        
        if ( phone.isEqualToString("") || name.isEqualToString("") || confPassword.isEqualToString("") || username.isEqualToString("") || password.isEqualToString("") ) {
        
            let alertView = UIAlertController(title: "Sign up Failed!", message: "Please fill the blank text", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
            self.presentViewController(alertView, animated: true){}
            readyRegis=false
            
        }
        
        print(password.isEqualToString(confPassword as String))
        print(!password.isEqualToString(confPassword as String))
        
        if(!password.isEqualToString(confPassword as String)){
            let alertView = UIAlertController(title: "Sign up Failed!", message: "Password and Confirm Password not match", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
            self.presentViewController(alertView, animated: true){}
            readyRegis=false
        }
        
        
        if(readyRegis){
            
            do{
                let passwordMd5:NSString = md5(string:password as String)
                let post:NSString = "role=2&nama=\(name)&no_telp=\(phone)&email=\(username)&password=\(passwordMd5)"
                
                NSLog("PostData: %@",post);
                
                let url:NSURL = NSURL(string: UrlService.signUp )!
                let postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
                
                let postLength:NSString = String( postData.length )
                
                let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "POST"
                request.HTTPBody = postData
                request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("assis", forHTTPHeaderField: "assis")
                
                
                LoadingHUD.showLoadingHUD(contentView)
                Alamofire.request(request).responseJSON {
                    response in
                    print(response.result)
                    switch response.result {
                    case .Success(let data):
                        let json = JSON(data)
                        print(json)
                        let status = json["status"].boolValue
                        if(status)
                        {
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: "Sign up Success!", message: "User has been Sign Up", preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default,handler:
                                { (action: UIAlertAction!) in


                                self.dismissViewControllerAnimated(true, completion: nil)
                                
                            }))
                            self.presentViewController(alertView, animated: true){}
                            
                        }
                        else
                        {
                             LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: "Sign up Failed!", message: json["msg"].stringValue, preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
                            self.presentViewController(alertView, animated: true){}
                        
                        }
    
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                }
                
            }
            
        }
        
          
    }
    
    
   
    @IBAction func gotoLogin(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
