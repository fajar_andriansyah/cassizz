//
//  UpdateProfileVC.swift
//  Cassizz
//
//  Created by TuanMudaSena on 13/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit
import Alamofire
import TextFieldEffects
import SwiftyJSON
import MBProgressHUD

class UpdateProfileVC: UIViewController {

    @IBOutlet var contentView: UIView!
    @IBOutlet var txtEmailUpdate: UITextField!
    @IBOutlet var txtNameUpdate: UITextField!
    @IBOutlet var txtPhoneUpdate: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtNameUpdate.text = User.getName()
        txtEmailUpdate.text = User.getEmail()
        txtPhoneUpdate.text = User.getPhone()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
//    id_user'=>$this->post('id_user'),
//    'username'=>$this->post('username'),
//    'email'=>$this->post('email'),
//    'no_telp'=>$this->post('no_telp')
    
    
    @IBAction func UpdateTapped(sender: UIButton) {
        
        
        let nameUpdate:NSString = txtNameUpdate.text!
        let emailUpdate:NSString = txtEmailUpdate.text!
        let phoneUpdate:NSString = txtPhoneUpdate.text!
        let idUser: NSString = User.getId()!
        
        var readyRegis:Bool = true
        
        
        if ( nameUpdate.isEqualToString("") || emailUpdate.isEqualToString("") || phoneUpdate.isEqualToString("") ) {
            
            let alertView = UIAlertController(title: "Update Failed!", message: "Please fill the blank text", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
            self.presentViewController(alertView, animated: true){}
            readyRegis=false
        }
        
        
        if(readyRegis){
            
            do{
                let post:NSString = "id_user=\(idUser)&username=\(nameUpdate)&no_telp=\(phoneUpdate)&email=\(emailUpdate)"
                NSLog("PostData: %@",post);
    
                let url:NSURL = NSURL(string:UrlService.updateProf)!
                
                let postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
                
                let postLength:NSString = String( postData.length )
                
                
                let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "POST"
                
                
                request.HTTPBody = postData
                request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("assis", forHTTPHeaderField: "assis")
                
                
                LoadingHUD.showLoadingHUD(contentView)
                Alamofire.request(request).responseJSON {
                    response in
                    print(response.result)
                    switch response.result {
                    case .Success(let data):
                        let json = JSON(data)
                        print(json)
                        let status = json["status"].boolValue
                        if(status)
                        {
                            User.setEmail(emailUpdate as String)
                            User.setName(nameUpdate as String)
                            User.setPhone(phoneUpdate as String)
                            
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: "Update Success!", message: "Profile has been Update", preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default,handler:
                                {(action: UIAlertAction!) in
                                    self.dismissViewControllerAnimated(true, completion: nil)
                            }))
                            self.presentViewController(alertView, animated: true){}
                        }
                        else
                        {
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let alertView = UIAlertController(title: "Update Failed!", message: json["msg"].stringValue, preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
                            self.presentViewController(alertView, animated: true){}
                        }
                        
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                }
                
            }
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
