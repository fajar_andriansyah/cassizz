//
//  Converter.swift
//  Cassizz
//
//  Created by TuanMudaSena on 21/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import Foundation

class  Converter {
    
    static func toString(obj : AnyObject) -> String? {
        if let value = obj as? String {
            return value
        }
        return nil
    }
    
    static func toInt(obj : AnyObject) -> Int? {
        if let value = obj as? Int {
            return value
        } else if let value = self.toString(obj) {
            return Int(value)
        }
        return nil
    }
    
    static func toBool(obj : AnyObject) -> Bool? {
        if let value = obj as? Bool {
            return value
        } else if let value = self.toInt(obj) {
            return Bool(value)
        }
        return nil
    }
}