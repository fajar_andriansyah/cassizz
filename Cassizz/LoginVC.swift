//
//  LoginVC.swift
//  Cassizz
//
//  Created by TuanMudaSena on 8/3/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import MBProgressHUD
import SwiftyJSON

class LoginVC: UIViewController{

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet var contentView: UIView!

    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func loginTapped(sender: UIButton) {
        let username:NSString = txtUserName.text!
        let password:NSString = txtPassword.text!
    
        
        
        //User For Test Login :
        //let username:NSString = "jatra1990@gmail.com"
        //let password:NSString = "123456"
        
        if ( username.isEqualToString("") || password.isEqualToString("") ) {
            
//            let alertView = UIAlertController(title: "Sign in Failed!", message: "Please enter Username and Password", preferredStyle: .Alert)
//            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
//            self.presentViewController(alertView, animated: true){}
            
            let controller = UIAlertController.alertControllerWithTitle("SignIn Failed", message: "Please enter Username and Password")
            self.presentViewController(controller, animated: true) {}
            
        } else {
            
            do {
                let passwordMd5:NSString = md5(string:password as String)
                let post:NSString = "username=\(username)&password=\(passwordMd5)"
                
                NSLog("PostData: %@",post);
                
                let url:NSURL = NSURL(string:UrlService.login)!
                let postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
                
                let postLength:NSString = String( postData.length )
                let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "POST"
                request.HTTPBody = postData
                request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("assis", forHTTPHeaderField: "assis")
                
                LoadingHUD.showLoadingHUD(contentView)
                
                Alamofire.request(request).responseJSON {
                    response in
                    print(response.result)
                    switch response.result {
                    case .Success(let data):
                        let json = JSON(data)
                        print(json)
                        let status = json["status"].boolValue
                        
        
                        if(status)
                        {
                            User.setLOGGEDIN()
                            LoadingHUD.hideLoadingHUD(self.contentView)
                            let dataVal = json["data"].arrayValue
                            let iduser = dataVal[0]["id_user"].stringValue
                            let noTelp = dataVal[0]["no_telp"].stringValue
                            let nama = dataVal[0]["nama"].stringValue
                            let email = dataVal[0]["email"].stringValue
                            
//                            let alertView = UIAlertController(title: "Signin Success!", message: "User has been Signin", preferredStyle: .Alert)
//                            alertView.addAction(UIAlertAction(title : "OK", style: .Default,handler:
//                                { (action: UIAlertAction!) in
//                                    self.dismissViewControllerAnimated(true, completion: nil)
//                            }))
//                            self.presentViewController(alertView, animated: true){}
//                            
                            self.dismissViewControllerAnimated(true, completion: nil)
                            
                            User.setId(iduser)
                            User.setName(nama)
                            User.setPhone(noTelp)
                            User.setEmail(email)
                        }
                        else
                        {
//                            let alertView = UIAlertController(title: "SignIn Failed", message: json["msg"].stringValue, preferredStyle: .Alert)
//                            alertView.addAction(UIAlertAction(title : "OK", style: .Default){ _ in})
//                            self.presentViewController(alertView, animated: true){}
                            
                            let controller = UIAlertController.alertControllerWithTitle("SignIn Failed", message: json["msg"].stringValue)
                            self.presentViewController(controller, animated: true) {}
                            
                            LoadingHUD.hideLoadingHUD(self.contentView)
                        }
                        
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                }
                
            }
        }
        
    }

    @IBAction func gotoSignup(sender: UIButton) {
//               self.performSegueWithIdentifier("goto_signup", sender: self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
