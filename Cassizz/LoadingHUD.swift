//
//  UtilCassizz.swift
//  Cassizz
//
//  Created by Sena on 3/12/16.
//  Copyright © 2016 TuanMudaSena. All rights reserved.
//

import Foundation
import MBProgressHUD
import UIKit

class LoadingHUD {
    
    static func showLoadingHUD(contentView:UIView) {
        let hud = MBProgressHUD.showHUDAddedTo(contentView, animated: true)
        hud.labelText = "Loading..."
    }
    
    static func hideLoadingHUD(contentView:UIView) {
        MBProgressHUD.hideAllHUDsForView(contentView, animated: true)
    }
    
    
}


